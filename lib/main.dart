import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/feature/counter/presentation/bloc/counter_bloc.dart';
import 'package:test_app/feature/theme/presentation/bloc/theme_bloc.dart';
import 'package:test_app/feature/weather/presentation/bloc/weather_bloc.dart';
import 'package:test_app/feature/weather/data/repository/weather_repository_impl.dart';
import 'package:test_app/home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CounterBloc>(
          create: (context) => CounterBloc(),
        ),
        BlocProvider<ThemeBloc>(
          create: (context) => ThemeBloc(),
        ),
        BlocProvider<WeatherBloc>(
          create: (context) => WeatherBloc(weatherRepositoryImpl: WeatherRepositoryImpl()),
        ),
      ],
      child: BlocBuilder<ThemeBloc, ThemeState>(
        builder: (context, state) {
          return MaterialApp(
            theme: state.themeData,
            home: const HomeScreen(title: 'Weather Counter'),
          );
        },
      ),
    );
  }
}
