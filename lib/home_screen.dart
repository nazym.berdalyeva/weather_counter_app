import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/feature/counter/presentation/bloc/counter_bloc.dart';
import 'package:test_app/feature/theme/presentation/bloc/theme_bloc.dart';
import 'package:test_app/feature/weather/presentation/bloc/weather_bloc.dart';
import 'package:test_app/feature/weather/presentation/screens/weather_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final CounterBloc counterBloc = BlocProvider.of<CounterBloc>(context);
    final ThemeBloc themeBloc = BlocProvider.of<ThemeBloc>(context);
    final WeatherBloc weatherBloc = BlocProvider.of<WeatherBloc>(context);
    return BlocBuilder<CounterBloc, CounterState>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const WeatherScreen(),
                const SizedBox(
                  height: 50,
                ),
                const Text(
                  'You have pushed the button this many times:',
                ),
                AnimatedSwitcher(
                  duration: const Duration(milliseconds: 500),
                  transitionBuilder: (Widget child, Animation<double> animation) {
                    return ScaleTransition(scale: animation, child: child);
                  },
                  child: Text(
                    '${state.counter}',
                    key: ValueKey<int>(state.counter),
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
              ],
            ),
          ),
          floatingActionButton: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 36.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FloatingActionButton(
                      onPressed: () => weatherBloc.add(GetWeather()),
                      tooltip: 'Weather',
                      child: const Icon(Icons.cloud),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    FloatingActionButton(
                      onPressed: () => themeBloc.add(ChangeTheme()),
                      tooltip: 'Theme',
                      child: AnimatedSwitcher(
                        duration: const Duration(milliseconds: 450),
                        child: Icon(themeBloc.state.isDarkThemeOn ? Icons.brightness_2 : Icons.sunny),
                      ),
                    ),
                  ],
                ),
              ),
              BlocBuilder<CounterBloc, CounterState>(builder: (context, state) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    AnimatedSwitcher(
                      duration: const Duration(milliseconds: 750),
                switchInCurve: Curves.easeInSine,
                      child: (state.showAddBtn)
                          ? FloatingActionButton(
                              onPressed: () => counterBloc.add((themeBloc.state.isDarkThemeOn) ? Increment(2) : Increment(1)),
                              tooltip: 'Increment',
                              child: const Icon(Icons.add),
                            )
                          : const SizedBox(
                              height: 56,
                            ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    AnimatedSwitcher(
                      duration: const Duration(milliseconds: 450),
                      child: (state.showMinusBtn)
                          ? FloatingActionButton(
                              onPressed: () => counterBloc.add((themeBloc.state.isDarkThemeOn) ? Decrement(2) : Decrement(1)),
                              tooltip: 'Decrement',
                              child: const Icon(Icons.remove),
                            )
                          : const SizedBox(
                              height: 56,
                            ),
                    )
                  ],
                );
              }),
            ],
          ), // This trailing comma makes auto-formatting nicer for build methods.
        );
      },
    );
  }
}
