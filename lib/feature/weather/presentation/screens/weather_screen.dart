import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/feature/weather/data/weather_entity.dart';
import 'package:test_app/feature/weather/presentation/bloc/weather_bloc.dart';

class WeatherScreen extends StatelessWidget {
  const WeatherScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(builder: (context, state) {
      if (state.appState == AppState.FINISHED_DOWNLOADING) {
        return Text("Weather for ${state.weatherEntity?.country}, ${state.weatherEntity?.areaName}: ${state.weatherEntity?.temperature}");
      }
      else if(state.appState == AppState.DOWNLOADING) {
        return _contentDownloading();
      } else {
        return _contentNotDownloaded();
      }
    });
  }

  Widget _contentDownloading() {
    return Container(
      margin: const EdgeInsets.all(25),
      child: Column(children: [
        const Text(
          'Fetching Weather...',
          style: TextStyle(fontSize: 20),
        ),
        Container(
            margin: const EdgeInsets.only(top: 50),
            child: const Center(child: CircularProgressIndicator(strokeWidth: 10)))
      ]),
    );
  }

  Widget _contentNotDownloaded() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const <Widget>[
          Text(
            'Press the icon to get your location',
          ),
        ],
      ),
    );
  }
}
