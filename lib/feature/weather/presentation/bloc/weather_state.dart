part of 'weather_bloc.dart';

class WeatherState {
  late AppState appState;
  late WeatherEntity? weatherEntity;

  WeatherState({required this.appState, this.weatherEntity});

  List<Object> get props => [appState, weatherEntity!];
}