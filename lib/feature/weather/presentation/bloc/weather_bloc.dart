import 'package:bloc/bloc.dart';
import 'package:test_app/feature/weather/data/repository/weather_repository_impl.dart';
import 'package:test_app/feature/weather/data/weather_entity.dart';

part 'weather_event.dart';

part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherBloc({required this.weatherRepositoryImpl}) : super(WeatherState(appState: AppState.NOT_DOWNLOADED)) {
    on<GetWeather>((event, emit) => _getWeather(emit));
  }

  WeatherRepositoryImpl weatherRepositoryImpl;

  _getWeather(Emitter<WeatherState> emit) async {
    emit(WeatherState(appState: AppState.DOWNLOADING));
    final response = await weatherRepositoryImpl.getWeather();
    emit(WeatherState(appState: AppState.FINISHED_DOWNLOADING, weatherEntity: response));
  }
}
