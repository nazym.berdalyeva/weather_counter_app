part of 'weather_bloc.dart';

abstract class WeatherEvent {
  const WeatherEvent();
}

class GetWeather extends WeatherEvent {}