import 'package:test_app/feature/weather/data/weather_entity.dart';
import 'package:test_app/feature/weather/domain/repository/weather_repository.dart';
import 'package:weather/weather.dart';

class WeatherRepositoryImpl implements WeatherRepository {
  WeatherRepositoryImpl();

  String key = 'ca1f00d71e21f32ae1ebc3c51772e156';
  late WeatherFactory ws = WeatherFactory(key);

  @override
  Future<WeatherEntity?> getWeather() async{
    Weather weather = await ws.currentWeatherByCityName("Almaty");
    final weatherEntity = WeatherEntity(areaName: weather.areaName, country: weather.country, temperature: weather.temperature);
    return weatherEntity;
  }
}