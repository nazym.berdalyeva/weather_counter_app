import 'package:weather/weather.dart';

enum AppState { NOT_DOWNLOADED, DOWNLOADING, FINISHED_DOWNLOADING }

class WeatherEntity {
  late String? country;
  late String? areaName;
  late Temperature? temperature;

  WeatherEntity({this.country, this.areaName, this.temperature});

  factory WeatherEntity.fromJson(Map<String, dynamic> json) => WeatherEntity(
    country: json["country"],
    areaName: json["areaName"],
    temperature: json["temperature"],
  );

}