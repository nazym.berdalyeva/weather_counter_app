
import 'package:test_app/feature/weather/data/weather_entity.dart';

abstract class WeatherRepository {
  Future<WeatherEntity?> getWeather();
}
