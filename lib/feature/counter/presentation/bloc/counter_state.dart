part of 'counter_bloc.dart';

class CounterState extends Equatable {
  final int counter;
  final bool showAddBtn;
  final bool showMinusBtn;

  const CounterState({
    required this.counter,
    this.showAddBtn = true,
    this.showMinusBtn = false,
  });

  @override
  List<Object> get props => [counter, showAddBtn, showMinusBtn];
}
