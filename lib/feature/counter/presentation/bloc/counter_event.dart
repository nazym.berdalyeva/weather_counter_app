part of 'counter_bloc.dart';

abstract class CounterEvent {
  const CounterEvent();
}

class Increment extends CounterEvent {
  final int num;

  Increment(this.num);
}

class Decrement extends CounterEvent {
  final int num;

  Decrement(this.num);
}