import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'counter_event.dart';

part 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  CounterBloc() : super(const CounterState(counter: 0)) {
    on<Decrement>((event, emit) => _onDecrement(event.num, emit));
    on<Increment>((event, emit) => _onIncrement(event.num, emit));
  }

  _onIncrement(int num, Emitter<CounterState> emit) {
    final addition = num + state.counter;
    if (addition == 10) {
      emit(CounterState(counter: addition, showAddBtn: false, showMinusBtn: true));
    } else if (addition > 10) {
      emit(const CounterState(counter: 10, showAddBtn: false, showMinusBtn: true));
    } else {
      emit(CounterState(counter: state.counter + num, showMinusBtn: true, showAddBtn: true));
    }
  }

  _onDecrement(int num, Emitter<CounterState> emit) {
    final subtraction = state.counter - num;
    if (subtraction == 0) {
      emit(CounterState(counter: subtraction, showAddBtn: true, showMinusBtn: false));
    } else if (subtraction < 0) {
      emit(const CounterState(counter: 0, showAddBtn: true, showMinusBtn: false));
    } else {
      emit(CounterState(counter: state.counter - num, showAddBtn: true, showMinusBtn: true));
    }
  }
}
