part of 'theme_bloc.dart';

class ThemeState {
  final bool isDarkThemeOn;
  late ThemeData themeData;

  ThemeState({this.isDarkThemeOn = false}) {
    if (isDarkThemeOn) {
      themeData = appThemeData[AppTheme.darkTheme]!;
    } else {
      themeData = appThemeData[AppTheme.lightTheme]!;
    }
  }

  List<Object> get props => [isDarkThemeOn];
}