import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:test_app/feature/theme/domain/entity/theme_data.dart';

part 'theme_event.dart';

part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc() : super(ThemeState(isDarkThemeOn: true)) {
    on<ChangeTheme>((event, emit) => _changeTheme(emit));
  }

  _changeTheme(Emitter<ThemeState> emit) {
    emit(ThemeState(isDarkThemeOn: !state.isDarkThemeOn));
  }
}
