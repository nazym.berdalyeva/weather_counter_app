import 'package:flutter/material.dart';

enum AppTheme {
  lightTheme,
  darkTheme,
}

final appThemeData = {
  AppTheme.darkTheme: ThemeData(
    brightness: Brightness.dark,
    scaffoldBackgroundColor: Colors.grey[800],
    textTheme: TextTheme(
      headline3: const TextStyle().copyWith(color: Colors.white),
    ),
  ),
  AppTheme.lightTheme: ThemeData(
    brightness: Brightness.light,
    scaffoldBackgroundColor: Colors.white,
    textTheme: TextTheme(
      headline3: const TextStyle().copyWith(color: Colors.black),
    ),
  ),
};